import logging
from logstash_async.handler import AsynchronousLogstashHandler
from logstash_async.handler import LogstashFormatter

logger = logging.getLogger("logstash")
logger.setLevel(logging.INFO)

# Create the handler
handler = AsynchronousLogstashHandler(
    host='elk.faishol.net',
    port=5000,
    ssl_enable=False,
    ssl_verify=False,
    database_path='')
# Here you can specify additional formatting on your log record/message
formatter = LogstashFormatter()
handler.setFormatter(formatter)

# Assign handler to the logger
logger.addHandler(handler)

def loggerInfo(message):
    logger.info("[Downloader] "+ message)
