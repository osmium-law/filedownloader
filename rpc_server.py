#!/usr/bin/env python
import pika
import json
from logger import loggerInfo

from filedownloader import file_downloader

param = pika.URLParameters("amqp://osmium:osmium12345678@rabbitmq.faishol.net:5672")
connection = pika.BlockingConnection(param)

channel = connection.channel()

channel = connection.channel()

channel.queue_declare(queue='downloader_queue')


def on_request(ch, method, props, body):
    n = body

    print(n)
    data = json.loads(body)
    response = file_downloader(data['url'], data['folder_out'])
    dataResponse = {
        "folder_out": data["folder_out"],
        "url": data["url"],
        "query_type": "download",
        "response": response
    }
    loggerInfo(json.dumps(dataResponse))
    ch.basic_publish(exchange='',
                     routing_key=props.reply_to,
                     properties=pika.BasicProperties(correlation_id= \
                                                         props.correlation_id),
                     body=json.dumps(dataResponse))
    ch.basic_ack(delivery_tag=method.delivery_tag)


channel.basic_qos(prefetch_count=1)
channel.basic_consume(queue='downloader_queue', on_message_callback=on_request)

print(" [x] Awaiting RPC requests")
channel.start_consuming()
