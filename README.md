# FileDownloader

repository untuk file downloader

# Prerequisites
*  Python

## Installation Guide
*   Make sure you are in this project directory in your local device.

*  Create virtual python environment and activate.
    ```
    python -m venv env
    ```
    *   If you are using Windows
        ```
        env/Scripts/activate.bat
        ```
    *   If you are using Linux
        ```
        source env/bin/activate
        ```

*   Install packages necessary for the app to run.
    ```
    pip install -r requirements.txt
    ```

*   Run the server.
    ```
    python rpc_server.py
    ```