import numpy as np
import requests
import re
import os
from logger import loggerInfo

def getID(url):
    regex = re.compile(r'[-\w]{25,}(?!.*[-\w]{25,})')
    parts = regex.search(url)
    if (parts == None) :
        return url
    else :
        return parts.group()


def file_downloader(url,path):
    id = getID(url)
    r = requests.post(
        "https://script.google.com/macros/s/AKfycbzkSZYNUu8sUp0EIi1MuyDVSrGi4CWCVCWC0MnYnWl6eROnLUo/exec",
        data={"id": id}
    )

    if(r.status_code == 500):
        loggerInfo("file ngak ketemu")
        return ("file ngak ketemu")
    else:
        if("Exception" in r.text):
            loggerInfo("file tidak bisa diakses")
            return ('file tidak bisa diakses')
        else:
            if not os.path.exists(path):
                os.makedirs(path)
            filepath = os.path.join(path, r.json()["name"])
            f = open(filepath, "bw")
            f.write(np.array(r.json()["result"], dtype=np.uint8))
            f.close()
            loggerInfo("Filename = {0}, MimeType = {1}, size = {2}".format(r.json()["name"], r.json()["mimeType"], r.json()["size"]))
            return ("Filename = {0}, MimeType = {1}, size = {2}".format(r.json()["name"], r.json()["mimeType"], r.json()["size"]))


